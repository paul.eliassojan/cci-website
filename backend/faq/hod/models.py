from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver

class HOD(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    branch_id = models.IntegerField()

@receiver(models.signals.post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if instance :
        user_instance=User.objects.get(pk=instance.pk)
        if not user_instance.is_superuser:
            hod_instance=HOD.objects.get(user=user_instance)
            if user_instance.username != hod_instance.name:
                hod_instance.name=user_instance.username
                hod_instance.save()

class Question(models.Model):
    query = models.CharField(max_length=300)
    branch_id = models.ForeignKey(HOD, on_delete=models.CASCADE)
    query_date = models.DateField('date asked')
    mail_id = models.CharField(max_length=45, default="default@gmail.com")
