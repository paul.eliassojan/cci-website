$(document).ready(function () {
  $('.modal').modal();
  $(".sidenav").sidenav();

  // Setting the initial state of the scroll buttons
  $('#left-button-dept').hide()
  $('#left-button-clubs').hide()
  $('#left-button-research').hide()
  $('#left-button-dept-mobile').addClass('ButtonClicked')
  $('#left-button-clubs-mobile').addClass('ButtonClicked')
  $('#left-button-research-mobile').addClass('ButtonClicked')
  $('#right-button-dept-mobile').addClass('ButtonUnclicked')
  $('#right-button-clubs-mobile').addClass('ButtonUnclicked')
  $('#right-button-research-mobile').addClass('ButtonUnclicked')

  // Slider for Departments
  $('#slide-dept').on("scroll", function() {
    if($('#slide-dept').scrollLeft() <= 0) {
      $('#left-button-dept').hide()
    }
    else {
      $('#left-button-dept').show()
    }
    if(document.getElementById('slide-dept').scrollWidth <= $('#right-button-dept').position().left + $('#slide-dept').scrollLeft() + 150){
      $('#right-button-dept').hide()
    }
    else {
      $('#right-button-dept').show()
    }
    if($('#slide-dept').scrollLeft() <= 0) {
      $('#left-button-dept-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#left-button-dept-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
    if(document.getElementById('slide-dept').scrollWidth - $('#slide-dept').scrollLeft() <= $(window).width()) {
      $('#right-button-dept-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#right-button-dept-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
  });
  
  // Slider for Clubs
  $('#slide-clubs').on("scroll", function() {
    if($('#slide-clubs').scrollLeft() <= 0) {
      $('#left-button-clubs').hide()
    }
    else {
      $('#left-button-clubs').show()
    }
    if(document.getElementById('slide-clubs').scrollWidth <= $('#right-button-clubs').position().left + $('#slide-clubs').scrollLeft() + 150){
      $('#right-button-clubs').hide()
    }
    else {
      $('#right-button-clubs').show()
    }
    if($('#slide-clubs').scrollLeft() <= 0) {
      $('#left-button-clubs-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#left-button-clubs-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
    if(document.getElementById('slide-clubs').scrollWidth - $('#slide-clubs').scrollLeft() <= $(window).width()) {
      $('#right-button-clubs-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#right-button-clubs-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
  });
  
  // Slider for Research
  $('#slide-research').on("scroll", function() {
    if($('#slide-research').scrollLeft() <= 0) {
      $('#left-button-research').hide()
    }
    else {
      $('#left-button-research').show()
    }
    if(document.getElementById('slide-research').scrollWidth <= $('#right-button-research').position().left + $('#slide-research').scrollLeft() + 150){
      $('#right-button-research').hide()
    }
    else {
      $('#right-button-research').show()
    }
    if($('#slide-research').scrollLeft() <= 0) {
      $('#left-button-research-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#left-button-research-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
    if(document.getElementById('slide-research').scrollWidth - $('#slide-research').scrollLeft() <= $(window).width()) {
      $('#right-button-research-mobile').removeClass('ButtonUnclicked').addClass('ButtonClicked')
    }
    else {
      $('#right-button-research-mobile').removeClass('ButtonClicked').addClass('ButtonUnclicked')
    }
  });

  // Scroll For Desktop

  // Departments
  $('#left-button-dept').click(function () {
    event.preventDefault();
    $('#slide-dept').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-dept').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-dept').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });

  // Clubs
  $('#left-button-clubs').click(function () {
    event.preventDefault();
    $('#slide-clubs').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-clubs').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-clubs').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });

  // Research
  $('#left-button-research').click(function () {
    event.preventDefault();
    $('#slide-research').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-research').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-research').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });


  // Scroll For Mobile

  // Departments
  $('#left-button-dept-mobile').click(function () {
    event.preventDefault();
    $('#slide-dept').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-dept-mobile').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-dept').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });

  // Clubs
  $('#left-button-clubs-mobile').click(function () {
    event.preventDefault();
    $('#slide-clubs').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-clubs-mobile').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-clubs').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });

  // Research
  $('#left-button-research-mobile').click(function () {
    event.preventDefault();
    $('#slide-research').animate({
      scrollLeft: "-=500px"
    }, "slow");
  });
  $('#right-button-research-mobile').click(function () {
    event.preventDefault();
    console.log("right");
    $('#slide-research').animate({
      scrollLeft: "+=500px"
    }, "slow");
  });

});


grecaptcha.ready(function () {
  grecaptcha.execute('6Lc495gUAAAAAOt8dYUsdyRPYLwHzaEw7Ud0Z4es', {
          action: 'success/'
      })
      .then(function (token) {
          var input = document.createElement('input');
          input.setAttribute("type", "hidden");
          input.setAttribute("name", "g-recaptcha-response");
          input.setAttribute("value", token);
          document.getElementById("form").appendChild(input);
      });


//Adding a prompt to add Homescreen[Chrome]
let deferredPrompt;
window.addEventListener('beforeinstallprompt',(e)=>{
  e.preventDefault();  
  deferredPrompt=e;
});
var installButton=document.getElementById('install-now-button');
var installBanner=document.getElementById('install-banner');
installButton.addEventListener('click', (e) => {
  // hide our user interface that shows our A2HS button
  // Show the prompt
  try {
    deferredPrompt.prompt();
  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
    .then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        installBanner.style.display='none';
      } else {
        console.log('User dismissed the A2HS prompt');
      }
      deferredPrompt = null;
    });
  } catch (error) {
    window.alert('App is already installed');
  }
  
});


});
