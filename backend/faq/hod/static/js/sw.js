var staticCacheName="Fisatian"+new Date().getTime().toString();
var filesTocache=[
    '/',
    'offline',
    '/static/images/logo.png'
];
//Adding files to cache
self.addEventListener("install",event=>{
    event.waitUntil(
        caches.open(staticCacheName)
        .then(cache=>{
            return cache.addAll(filesTocache);
        })
    );
});
//Deleting cache on active Connection
self.addEventListener('active',event=>{
    event.waitUntil(
        caches.keys().then(
            cacheNames=>{
                return Promise.all(
                    cacheNames
                    .filter(cacheName=>(cacheName.startsWith("Fisatian")))
                    .filter(cacheName=>(cacheName!==staticCacheName))
                    .map(cacheName=>caches.delete(cacheName))
                );
            }
        )
    );
});
//Serve from Cache
self.addEventListener('fetch',event=>{
    event.respondWith(
        caches.match(event.request)
        .then(response=>{
            return response || fetch(event.request);
        })
        .catch(()=>{
            return caches.match('offline');
        })
    );
});
