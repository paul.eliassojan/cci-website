from django.forms import ModelForm
from .models import *

class QueryForm(ModelForm):
    class Meta:
        model=Question
        fields=[ 'query' , 'mail_id' ]
        exclude=('branch_id','query_date',)