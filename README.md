# CCI

Official Fisat App

# Setting Up Your Environment

The instruction here will be mostly ubuntu focused since most of you guys are using it.

### Front end

To be added :)

### Back end

Make sure you have python3 and pip for python3. Then install pipenv

```
sudo pip3 install pipenv
```

#### Postgresql setup

(tested with ubuntu 18.04)

Install postgresql

```
$ sudo apt install postgresql
```

Creating user

```
$ sudo -i -u postgres
postgres@server:~$ createuser --interactive -P
```

Setting up password

```
postgres@server:~$psql
# ALTER USER $USERNAME WITH PASSWORD 'new_password';
```

username, password and database name must be obtained from ./backend/faq/faq/settings.py
The user should be a superuser.

```
postgres@server:~$ createdb -O $USERNAME $DATABASE_NAME
```

Restoring data from backup file

```
psql -U $USERNAME $DATABASE_NAME < ./backend/faq/faq/backup.sql
```

If you get the error

```
psql:FATAL" Peer authentication failed for user "$USERNAME"
```

Edit the file `$ etc/postgresql/10/main/pg_hba.conf` and edit line

```
local       all         all      peer
```

TO

```
local       all         all        md5
```

Then restart the service by

```
$ sudo systemctl restart postgresql
$ /etc/init.d/postgresql reload
```

## Make port 80 avaiable in firewall

**Some systems have firewall which blocks 80\* ports. Thus we have to manually enable it.**  
Thus if your system has `ufw` installed and running, then do the following :-
```
sudo ufw enable
sudo ufw allow 80
```

## Running Django

### Make the migrations

```
cd ./backend/faq
pipenv install
pipenv shell
python3 manage.py makemigrations
python3 manage.py migrate
```

### Find local system IP

Type in the following command find your system ip :-

```
ip addr show `ip addr | grep -i "state up" | awk '{print $2}' | tr -d ':'` | grep "inet\b" | awk '{print $2}' | cut -d/ -f1
```

if that doesn't work then type in `ip addr` and manually find it out.

**Use this ip addr while running the server along with port 8000, like eg:- 192.102.16.1:8000**

## To run the server

**assuming that you're still in the pipenv shell**  

```
cd ./backend/faq
python3 manage.py runserver the_ip_you_found_out_before:8000

# or do it this way
python3 manage.py runserver $(ip addr show `ip addr | grep -i "state up" | awk '{print $2}' | tr -d ':'` | grep "inet\b" | awk '{print $2}' | cut -d/ -f1):8000
```

And open browser and browse [127.0.0.1:8000](127.0.0.1:8000) or the_ip_you_found_out_before:8000

# Issues

Raise an issue while finding bugs, adding new features and for suggesting enhancements.
