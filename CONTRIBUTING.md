# Contributing

When contributing or changing anything please be use to discuses with your team via Discord or Whatsapp.
Make sure to fork the master repo and use branches to implement fixes and features.


## Pull Request Process

### 1. Explore

If there is some issue or bug or enhancements, you are interested in or have been assigned, you may take it up by leaving a comment in the issue.

**Make sure you create an issue before making major code changes or adding new features**

### 2. Fork & create a branch



The core code for getting the website up and running should be made against master.

For adding new features/fixing issues, a good branch name would be (where issue #13 is the ticket you're working on):

```sh
git checkout -b 13-add-xyz-feature
```

### 3. Implement your fix or feature

At this point, you're ready to make your changes! Feel free to ask for help;



### Note 1

**make sure that you dont make too many code changes at once and later add everything as a single commit. by adding a new function or by fixing a relevant bug or by adding a new feature, make sure that you commit those chunks of code**

### Note 2
**kindly add descriptive commit messages. make it elaborate and on point. "fixes errors" is an example for a bad commit message. whereas "fix #331 by removing the call to FuncName() at line 22" is an example of a good commit message.**


### 4. Test for all the checks

Your patch should follow the same conventions & pass the same code quality
checks(i.e style guide) as the rest of the project.

#### Style guide for front end

1. Google style guide for [html/css](https://google.github.io/styleguide/htmlcssguide.html)

2. Google style guide for [javascript](https://google.github.io/styleguide/javaguide.html).

#### Style guide for back end

 This project follows [pep8](https://www.python.org/dev/peps/pep-0008/).

 To get pep8 as your linit system in VScode [follow this guide](https://code.visualstudio.com/docs/python/linting#_pep8-pycodestyle) or you could use this extension to make your work easier [autopep8.](https://marketplace.visualstudio.com/items?itemName=himanoa.Python-autopep8)

### 5. Make a Pull Request

At this point, you should switch back to your master branch and make sure it's
up to date with Active Admin's master branch:

```sh
git remote add upstream git@gitlab.com:cci-fisat/fisitian.git
git checkout master
git pull upstream master
```


Then update your feature branch from your local copy of master, and push it!

```sh
git checkout 13-add-xyz-feature
git rebase master
git push --set-upstream origin 13-add-xyz-feature
```

Finally, go to Gitlab and make a Pull Request :D